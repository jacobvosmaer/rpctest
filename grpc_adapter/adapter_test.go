package grpc_adapter

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/jacobvosmaer-gitlab/rpctest/transport"

	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	gitalyauth "gitlab.com/gitlab-org/gitaly/auth"
	"google.golang.org/grpc/examples/helloworld/helloworld"
	"google.golang.org/grpc/metadata"
)

func TestAdapter(t *testing.T) {
	logEntry := logrus.NewEntry(logrus.StandardLogger())
	startServer(t, NewServer(grpc_logrus.UnaryServerInterceptor(logEntry)))

	// Client: instantiate call
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctx = metadata.AppendToOutgoingContext(ctx, "hello-key", "world-value")
	c, err := Call(
		ctx,
		transport.DialNet("unix", testUnixAddr),
		"/helloworld.Greeter/SayHello",
		&helloworld.HelloRequest{Name: "Jane Doe"},
		WithClientInterceptor(
			grpc_logrus.UnaryClientInterceptor(
				logEntry,
				grpc_logrus.WithLevels(grpc_logrus.DefaultCodeToLevel),
			),
		),
	)
	require.NoError(t, err)
	defer c.Close()

	// Client: do random things with connection
	_, err = io.WriteString(c, "foobar")
	require.NoError(t, err)
	require.NoError(t, c.(*net.UnixConn).CloseWrite())
	_, err = io.Copy(os.Stderr, c)
	require.NoError(t, err)
}

func startServer(t *testing.T, s *Server) {
	t.Helper()
	helloworld.RegisterGreeterServer(s, &server{})
	os.Remove(testUnixAddr)
	require.NoError(t, os.MkdirAll(filepath.Dir(testUnixAddr), 0755))
	l, err := net.Listen("unix", testUnixAddr)
	require.NoError(t, err)
	t.Cleanup(func() { l.Close() })
	go s.Serve(l)
}

const testUnixAddr = "testdata/sock"

func TestAdapterAuth(t *testing.T) {
	const token = "secret token"
	authMiddleware := grpc_auth.UnaryServerInterceptor(func(ctx context.Context) (context.Context, error) {
		return ctx, gitalyauth.CheckToken(ctx, token, time.Now())
	})

	startServer(t, NewServer(authMiddleware))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	c, err := Call(
		ctx,
		transport.DialNet("unix", testUnixAddr),
		"/helloworld.Greeter/SayHello",
		&helloworld.HelloRequest{Name: "Jane Doe"},
		WithCredentials(gitalyauth.RPCCredentialsV2(token)),
	)
	require.NoError(t, err)
	require.NoError(t, c.Close())
}

// server is used to implement helloworld.GreeterServer.
type server struct {
	helloworld.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(ctx context.Context, in *helloworld.HelloRequest) (*helloworld.HelloReply, error) {
	// Server: validate request
	log.Printf("%s says hello", in.GetName())
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, errors.New("no metadata")
	}
	log.Printf("metadata: %v", md)

	c, err := AcceptConnection(ctx)
	if err != nil {
		return nil, err
	}
	log.Print("server accepted connection")

	// Server: do random things with connection
	echo, err := ioutil.ReadAll(c)
	if err != nil {
		return nil, err
	}

	_, err = fmt.Fprintf(c, "hello!!\nyou wrote: %s\n", echo)
	return nil, err
}
