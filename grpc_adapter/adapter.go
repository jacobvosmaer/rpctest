package grpc_adapter

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net"

	"gitlab.com/jacobvosmaer-gitlab/rpctest/transport"

	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

type request struct {
	Method   string
	Message  []byte
	Metadata map[string][]string
}

type callOptions struct {
	creds       credentials.PerRPCCredentials
	interceptor grpc.UnaryClientInterceptor
}

func (opts *callOptions) addCredentials(ctx context.Context) (context.Context, error) {
	headers, err := opts.creds.GetRequestMetadata(ctx)
	if err != nil {
		return nil, err
	}
	for k, v := range headers {
		ctx = metadata.AppendToOutgoingContext(ctx, k, v)
	}
	return ctx, nil
}

type CallOption func(*callOptions)

type nullCredentials struct{}

func (nullCredentials) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return nil, nil
}
func (nullCredentials) RequireTransportSecurity() bool { return false }

func nullClientInterceptor(ctx context.Context, method string, req, resp interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
	return invoker(ctx, method, req, resp, cc, opts...)
}

func defaultCallOptions() *callOptions {
	return &callOptions{
		creds:       nullCredentials{},
		interceptor: nullClientInterceptor,
	}
}

func WithCredentials(creds credentials.PerRPCCredentials) CallOption {
	return func(o *callOptions) { o.creds = creds }
}

func WithClientInterceptor(interceptor grpc.UnaryClientInterceptor) CallOption {
	return func(o *callOptions) { o.interceptor = interceptor }
}

func Call(ctx context.Context, dial transport.DialFunc, method string, msg proto.Message, opts ...CallOption) (c net.Conn, err error) {
	defer func() {
		if err != nil && c != nil {
			c.Close()
		}
	}()

	callOpts := defaultCallOptions()
	for _, o := range opts {
		o(callOpts)
	}

	invoke := func(ctx context.Context, method string, msg, _ interface{}, _ *grpc.ClientConn, _ ...grpc.CallOption) error {
		ctx, err := callOpts.addCredentials(ctx)
		if err != nil {
			return err
		}

		msgBytes, err := proto.Marshal(msg.(proto.Message))
		if err != nil {
			return err
		}

		req := &request{
			Method:  method,
			Message: msgBytes,
		}
		if md, ok := metadata.FromOutgoingContext(ctx); ok {
			req.Metadata = md
		}

		reqBytes, err := json.Marshal(req)
		if err != nil {
			return err
		}

		c, err = transport.Call(dial, reqBytes)
		return err
	}

	if err := callOpts.interceptor(ctx, method, msg, nil, nil, invoke); err != nil {
		return nil, err
	}

	return c, nil
}

type method struct {
	*grpc.MethodDesc
	implementation interface{}
}

type Server struct {
	*transport.Server
	methods     map[string]*method
	interceptor grpc.UnaryServerInterceptor
}

func NewServer(interceptor grpc.UnaryServerInterceptor) *Server {
	s := &Server{
		interceptor: interceptor,
	}
	s.Server = transport.NewServer(s.handle)
	return s
}

func (s *Server) RegisterService(sd *grpc.ServiceDesc, impl interface{}) {
	if s.methods == nil {
		s.methods = make(map[string]*method)
	}
	for _, m := range sd.Methods {
		s.methods["/"+sd.ServiceName+"/"+m.MethodName] = &method{
			MethodDesc:     &m,
			implementation: impl,
		}
	}
}

func serverContext(session *transport.ServerSession, req *request) (context.Context, func()) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, sessionKey, session)
	ctx = metadata.NewIncomingContext(ctx, req.Metadata)
	return context.WithCancel(ctx)
}

func (s *Server) handle(session *transport.ServerSession, reqBytes []byte) error {
	req := &request{}
	if err := json.Unmarshal(reqBytes, req); err != nil {
		return err
	}

	method, ok := s.methods[req.Method]
	if !ok {
		return fmt.Errorf("method not found: %s", req.Method)
	}

	ctx, cancel := serverContext(session, req)
	defer cancel()

	if _, err := method.Handler(
		method.implementation,
		ctx,
		func(msg interface{}) error { return proto.Unmarshal(req.Message, msg.(proto.Message)) },
		s.interceptor,
	); err != nil {
		return err
	}

	return nil
}

var sessionKey = &struct{}{}

func AcceptConnection(ctx context.Context) (net.Conn, error) {
	session, ok := ctx.Value(sessionKey).(*transport.ServerSession)
	if !ok {
		return nil, errors.New("context has no ServerSession")
	}
	return session.Accept()
}
