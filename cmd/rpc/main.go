package main

import (
	"bufio"
	"context"
	"crypto/sha256"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/exec"
	"strconv"
	"sync"

	"gitlab.com/jacobvosmaer-gitlab/rpctest/transport"
)

var listenAddr string

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: rpc LISTEN_ADDR")
		os.Exit(1)
	}

	go func() { // pprof
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	listenAddr = os.Args[1]

	if err := rpcServer(); err != nil {
		log.Fatal(err)
	}
}

func rpcServer() error {
	l, err := net.Listen("tcp", listenAddr)
	if err != nil {
		return err
	}
	defer l.Close()

	return transport.NewServer(handle).Serve(l)
}

func handle(s *transport.ServerSession, req []byte) error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	log.Printf("rpc: %s", req)
	h, ok := handlers[string(req)]
	if !ok {
		return fmt.Errorf("rpc not found: %s", req)
	}

	c, err := s.Accept()
	if err != nil {
		return err
	}

	log.Printf("rpc: %s: %v", req, h(ctx, c))
	return nil
}

var cacheMutex sync.Mutex

var handlers = map[string]func(context.Context, net.Conn) error{
	"GetInfoRefs": func(ctx context.Context, c net.Conn) error {
		cmd := exec.CommandContext(
			ctx,
			"git", "upload-pack", "--stateless-rpc", "--advertise-refs", ".",
		)
		cmd.Stdout = c
		if f, ok := getFile(c); ok {
			defer f.Close()
			cmd.Stdout = f
			log.Print("GetInfoRefs: using zero copy")
		}
		return cmd.Run()
	},
	"PostUploadPack": func(ctx context.Context, c net.Conn) error {
		stdin, err := getSizeReader(bufio.NewReader(c))
		if err != nil {
			return err
		}

		args := []string{"-c", "uploadpack.packObjectsHook=" + os.Getenv("HOOK_PATH"), "upload-pack", "--stateless-rpc", "."}
		cmd := exec.CommandContext(ctx, "git", args...)
		cmd.Stdin = stdin

		cmd.Stdout = c
		if f, ok := getFile(c); ok {
			defer f.Close()
			cmd.Stdout = f
			log.Print("PostUploadPack: using zero copy")
		}

		cmd.Stderr = os.Stderr
		cmd.Env = append(os.Environ(), "RPC_ADDR="+listenAddr)
		return cmd.Run()
	},
	"PackObjectsHook": func(ctx context.Context, c net.Conn) error {
		h := sha256.New()
		r := bufio.NewReader(io.TeeReader(c, h))
		line, err := r.ReadBytes('\n')
		if err != nil {
			return err
		}
		var args []string
		if err := json.Unmarshal(line, &args); err != nil {
			return err
		}
		stdin, err := getSizeReader(r)
		if err != nil {
			return err
		}

		if os.Getenv("USE_CACHE") != "1" {
			mux := &streamMux{w: c}

			cmd := exec.CommandContext(ctx, args[0], args[1:]...)
			cmd.Stdin = stdin
			cmd.Stdout = &streamMuxWriter{mux, 1}
			cmd.Stderr = &streamMuxWriter{mux, 2}
			if err := cmd.Run(); err != nil {
				return err
			}
			return mux.Close()
		}

		const cacheDir = "/tmp/pack-objects-cache"
		if err := os.MkdirAll(cacheDir, 0700); err != nil {
			return err
		}

		tmp, err := ioutil.TempFile(cacheDir, "stdin")
		if err != nil {
			return err
		}
		defer tmp.Close()
		if err := os.Remove(tmp.Name()); err != nil {
			return err
		}
		if _, err := io.Copy(tmp, stdin); err != nil {
			return err
		}
		cachePath := fmt.Sprintf("%s/%x", cacheDir, string(h.Sum(nil)))
		if f, err := os.Open(cachePath); err == nil {
			defer f.Close()
			log.Print("PackObjectsHook: cache hit")
			_, err := io.Copy(c, f)
			return err
		}

		cacheMutex.Lock()
		defer cacheMutex.Unlock()
		if _, err := tmp.Seek(0, io.SeekStart); err != nil {
			return err
		}

		f, err := ioutil.TempFile(cacheDir, "stdout")
		if err != nil {
			return err
		}
		defer f.Close()
		defer os.Remove(f.Name())

		mux := &streamMux{w: io.MultiWriter(f, c)}

		cmd := exec.CommandContext(ctx, args[0], args[1:]...)
		cmd.Stdin = tmp
		cmd.Stdout = &streamMuxWriter{mux, 1}
		cmd.Stderr = &streamMuxWriter{mux, 2}
		if err := cmd.Run(); err != nil {
			return err
		}

		if err := mux.Close(); err != nil {
			return err
		}

		if err := f.Close(); err != nil {
			return err
		}

		return os.Rename(f.Name(), cachePath)
	},
}

type filer interface{ File() (*os.File, error) }

func getFile(c net.Conn) (*os.File, bool) {
	if os.Getenv("ZERO_COPY") != "1" {
		return nil, false
	}

	ff, ok := c.(filer)
	if !ok {
		return nil, false
	}
	f, err := ff.File()
	if err != nil {
		return nil, false
	}
	return f, true
}

type streamMux struct {
	sync.Mutex
	w io.Writer
}

func (sm *streamMux) Close() error {
	sm.Lock()
	defer sm.Unlock()
	_, err := sm.w.Write([]byte{0, 0, 0})
	return err
}

func (sm *streamMux) WriteStream(id byte, p []byte) (int, error) {
	sm.Lock()
	defer sm.Unlock()

	var n int
	for len(p) > 0 {
		var hdr [3]byte
		buf := p
		if len(buf) > math.MaxUint16 {
			buf = buf[:math.MaxUint16]
		}
		binary.BigEndian.PutUint16(hdr[:2], uint16(len(buf)))
		hdr[2] = id
		if _, err := sm.w.Write(hdr[:]); err != nil {
			return n, err
		}
		m, err := sm.w.Write(buf)
		if m != len(buf) {
			return n, io.ErrShortWrite
		}
		if err != nil {
			return n, err
		}

		p = p[m:]
		n += m
	}

	return n, nil
}

type streamMuxWriter struct {
	*streamMux
	id byte
}

func (smw *streamMuxWriter) Write(p []byte) (int, error) { return smw.WriteStream(smw.id, p) }

func getSizeReader(r *bufio.Reader) (io.Reader, error) {
	header, err := r.ReadString('\n')
	if err != nil {
		return nil, err
	}
	size, err := strconv.ParseInt(header[:len(header)-1], 10, 64)
	if err != nil {
		return nil, err
	}

	return io.LimitReader(r, size), nil
}
