package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"os"

	"gitlab.com/jacobvosmaer-gitlab/rpctest/transport"
)

func main() {
	if err := _main(os.Getenv("RPC_ADDR")); err != nil {
		log.Fatal(err)
	}
}

func _main(rpcAddr string) error {
	f, err := ioutil.TempFile("", "hook-stdin")
	if err != nil {
		return err
	}
	defer f.Close()
	n, err := io.Copy(f, os.Stdin)
	if err != nil {
		return err
	}
	if _, err := f.Seek(0, io.SeekStart); err != nil {
		return err
	}

	c, err := transport.Call(transport.DialNet("tcp", rpcAddr), []byte("PackObjectsHook"))
	if err != nil {
		return err
	}
	defer c.Close()

	args, err := json.Marshal(os.Args[1:])
	if err != nil {
		return err
	}
	if _, err := fmt.Fprintf(c, "%s\n%d\n", args, n); err != nil {
		return err
	}

	if _, err := io.Copy(c, f); err != nil {
		return err
	}

	hdr := make([]byte, 3)
	buf := make([]byte, math.MaxUint16)
	for {
		_, err := io.ReadFull(c, hdr)
		if err != nil {
			return err
		}
		if bytes.Equal(hdr, []byte{0, 0, 0}) {
			return nil
		}

		var w io.Writer
		switch hdr[2] {
		case 1:
			w = os.Stdout
		case 2:
			w = os.Stderr
		default:
			return fmt.Errorf("invalid band: %x", buf[:3])
		}
		p := buf[:binary.BigEndian.Uint16(hdr)]
		if _, err := io.ReadFull(c, p); err != nil {
			return err
		}
		if _, err := w.Write(p); err != nil {
			return err
		}
	}

	return err
}
