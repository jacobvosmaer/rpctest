package main

import (
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/jacobvosmaer-gitlab/rpctest/transport"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: frontend LISTEN_ADDR RPC_ADDR")
		os.Exit(1)
	}

	if err := httpServer(os.Args[1], os.Args[2]); err != nil {
		log.Fatal(err)
	}
}

func httpServer(listenAddr string, rpcAddr string) error {
	http.HandleFunc("/repo.git/info/refs", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("frontend: info/refs: %v", handleGet(w, r, rpcAddr))
	})
	http.HandleFunc("/repo.git/git-upload-pack", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("frontend: git-upload-pack: %v", handlePost(w, r, rpcAddr))
	})

	return http.ListenAndServe(listenAddr, nil)
}

func handleGet(w http.ResponseWriter, r *http.Request, rpcAddr string) error {
	c, err := transport.Call(transport.DialNet("tcp", rpcAddr), []byte("GetInfoRefs"))
	if err != nil {
		return err
	}
	defer c.Close()

	w.Header().Set("Content-Type", "application/x-git-upload-pack-advertisement")
	if _, err := io.WriteString(w, "001e# service=git-upload-pack\n0000"); err != nil {
		return err
	}
	_, err = io.Copy(w, c)
	return err
}

func handlePost(w http.ResponseWriter, r *http.Request, rpcAddr string) error {
	c, err := transport.Call(transport.DialNet("tcp", rpcAddr), []byte("PostUploadPack"))
	if err != nil {
		return err
	}
	defer c.Close()

	defer r.Body.Close()
	body := r.Body
	if r.Header.Get("content-encoding") == "gzip" {
		z, err := gzip.NewReader(r.Body)
		if err != nil {
			return err
		}
		body = z
		defer z.Close()
	}

	f, err := ioutil.TempFile("", "frontend-stdin")
	if err != nil {
		return err
	}
	defer f.Close()
	if err := os.Remove(f.Name()); err != nil {
		return err
	}

	size, err := io.Copy(f, body)
	if err != nil {
		return err
	}

	if _, err := f.Seek(0, io.SeekStart); err != nil {
		return err
	}

	if _, err := fmt.Fprintf(c, "%d\n", size); err != nil {
		return err
	}
	if _, err := io.Copy(c, f); err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/x-git-upload-pack-result")
	_, err = io.Copy(w, c)
	return err
}
